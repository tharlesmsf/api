# API - Utilizando Express

Este projeto tem a finalidade aprender a desenvolver api utilizando node da melhor forma possível. 

# Requisitos

- node >= v6.9.0 
- npm >= v3

# Instalação

Faça o clone deste projeto:

```
git clone https://gitlab.com/tharlesmsf/api.git
```

Acesse a pasta do projeto:

```
cd api
```

## Dependências

Assumindo que você já tem o node e npm devidamente instalados, conforme os requisitos a cima, precisamos apenas instalar o vs code.

# Abrindo o projeto


